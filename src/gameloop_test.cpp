//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include "GameLoop.h"

namespace fm = muncher;

int main ()
{


    //fm::GameLoop g = fm::GameLoop(wd, wn);

    //std::cout << c->sprite->getPosition().y << std::endl;

    fm::GameLoop g;
    b2Vec2 worldSize = b2Vec2(1000, 1000);
    g.setWorldSize(worldSize);
    g.setScale(10.0f);
    g.runLoop();

    return 0;
}
