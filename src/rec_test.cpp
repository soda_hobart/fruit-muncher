#include <stdio.h>
#include <iostream>
#include <string>
extern "C" {
#include "rec.h"
}

int main (int argc, char* argv[])
{

    void rec_init (void);

    std::cout << argv[1] << std::endl;

    FILE * recfile;
    recfile = fopen(argv[1], "r");

    rec_parser_t p = rec_parser_new(recfile, argv[1]);

    rec_db_t data = rec_db_new();
    bool success = rec_parse_db(p, &data);
    std::cout << success << std::endl;
    if (success)
    {
        std::cout << "success" << std::endl;
    }
    else
    {
        printf("%s", "failed");
        std::cout << "failed" << std::endl;
    }

    rec_rset_t q = rec_db_query(
                data,
                "Texture",
                NULL,
                NULL,
                NULL,
                NULL,
                10,
                NULL,
                NULL,
                NULL,
                NULL,
                true);


    //std::string arg2(argv[2]);
    //int arg3 = std::stoi(argv[3]);
    rec_mset_t m = rec_rset_mset(q);

    rec_mset_dump(m);

    char field_str[] = "Map";
    rec_mset_type_t t = rec_mset_register_type(
                m,
                field_str,
                NULL,
                NULL,
                NULL,
                NULL);
    std::cout << t << std::endl;
    std::cout << "wuh?" << std::endl;


    void* d = rec_mset_get_at(m, t, 1);
    printf("%s", ( (char*) d));


    const char* v = static_cast<char*>(d);

    size_t n = sizeof(v)/sizeof(v[0]);
    std::cout << n << std::endl;

    std::cout << "wuh?" << std::endl;
    printf("%s", d);
    printf("%s", v);
    //std::string val = static_cast<std::string>(v);
    std::cout << v << std::endl;

    //void rec_fini (void);

    return 0;
}

