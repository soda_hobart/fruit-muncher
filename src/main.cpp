//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <queue>
#include <SFML/Graphics.hpp>
#include "Animation.h"

namespace fm = muncher;

int main()
{
  std::cout << "hey bud" << std::endl;

  // create the window
  sf::RenderWindow window(sf::VideoMode(800, 600), "My window");
  window.setVerticalSyncEnabled(true);

  // create the clock
  sf::Clock gameClock;
  sf::Time prevTime = gameClock.getElapsedTime();
  sf::Clock clock;

  sf::Texture texture;
  texture.loadFromFile("resources/tiles.png");
  sf::Sprite sprite;
  sprite.setTexture(texture);
  sf::IntRect position = sf::IntRect(0.0f, 0.0f, 100.0f, 100.0f);
  fm::AnimationSequencer a (10, 5, position);

  std::queue<sf::Event> inputEvent;


  // run the program as long as the window is open
  while (window.isOpen())
    {
      // check all the window's events that were triggered
      // since the last iteration of the loop
      sf::Event event;
      while (window.pollEvent(event))
        {
          // "close requested" event: we close the window
          if (event.type == sf::Event::Closed)
            window.close();

          if ((event.type == sf::Event::KeyPressed)
              && (event.key.code == sf::Keyboard::Enter))
            {
              inputEvent.push(event);
            }
        }
      sf::Time elapsed = clock.getElapsedTime();
      if (elapsed.asSeconds() > 0.05f)
        {
          // do stuff
          sf::Time gameTime = gameClock.getElapsedTime();
          if (gameTime.asSeconds() - prevTime.asSeconds() > 10)
            {
              prevTime = gameClock.getElapsedTime();
              int speed = a.getSpeed();
              if (speed > 0)
                {
                  a.setSpeed(--speed);
                }
              else
                {
                  window.close();
                  std::cout << "ok see you later." << std::endl;
                }
            }
          sf::IntRect texturePosition = a.getFramePosition();
          sprite.setPosition(100, 100);
          sprite.setTextureRect(texturePosition);
          clock.restart();
        }

      // clear the window with black color
      window.clear(sf::Color::Black);

      // draw everything here...
      // window.draw(...);
      window.draw(sprite);
      // end the current frame
      window.display();
    }

  return 0;
}
