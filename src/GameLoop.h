//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GameLoop_H
#define GameLoop_H

#include <map>
#include <string>
#include <queue>
#include <vector>
#include <list>
#include <utility>

#include <SFML/Graphics.hpp>
#include "Box2D/Box2D.h"
#include "rec.h"

#include "Animation.h"
#include "Collidable.h"
#include "Event.hpp"
#include "CollidableBuilder.h"


namespace fm = muncher;
namespace muncher {

// fwd decl
class xmlLoader {};

class GameLoop
{
public:
    GameLoop () {}
    //GameLoop(b2World* wld, sf::RenderWindow* win);
    virtual ~GameLoop () {}

    // events
    std::queue <fm::Event> eventQueue;
    bool pollEvent(fm::Event&);

    // renderables
    std::map<std::string, sf::Texture> textures;
    std::map<std::string, fm::Animation*> animations;
    void makeTexture (std::string, std::string);
    void makeAnimation (
            std::string, int[], int, int, std::string);

    void loadTextures ();
    void makeAnimations ();

    // world
    void initWorld (float, float, float, float);
    // TODO: remove these setters once a factory has been set up
    void setWorldSize(b2Vec2 size) {m_worldSize = size;}
    void setScale(float scale) {m_scale = scale;}

    void setAssetData (std::string);

    // collidables
    void makeCollidable(
            sf::Vector2f,
            sf::Vector2f,
            std::string,
            std::string,
            float,
            float,
            bool);

    // player
    fm::Collidable* getDudeman();
    void setDudeman(fm::Collidable*);

    //wrt vector transformations
    sf::Transform getLocalTransform();

    // clocks
    float graphicsRate = 0.05f;
    float physicsRate = 0.016f;


    void runLoop ();
protected:
    void pushEvent(fm::Event);
    bool popEvent(fm::Event&);
    void processEvents();
    void processInputEvents(sf::Event);
    void processPhysicsEvents();
    void processScriptedEvents();
private:
    // loads init data from xml
    xmlLoader * dataLoader;

    // world
    b2Vec2 m_worldSize;
    float m_scale;
    b2World * m_world;
    // collidables
    std::list<Collidable*> m_collidables;
    //sprites
    std::queue<sf::Sprite> m_sprites;
    // window
    sf::RenderWindow* m_window = new sf::RenderWindow(
                sf::VideoMode(800, 600), "Fruit Muncher");
    //sf::RenderWindow* m_window;

    // TODO: create some sort of logic for `dudeman`,
    // ie. not just hardcoding the player-controlled sprite
    // into the main loop.  But hardcoding is good for now,
    // of course.
    fm::Collidable m_dudeman;

    std::string m_assetDataPath;
};

class GameLoopFactory
{
    // just hard-coding the settings, at first
public:
    GameLoopFactory() {}
    GameLoop makeGameLoop ();
    void loadTextures ();
    void makeCollidables ();
};
}

#endif // GameLoop_H
