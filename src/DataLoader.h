#ifndef DATALOADER_H
#define DATALOADER_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "tinyxml2.h"

#include "GameLoop.h"

namespace fm = muncher;
namespace muncher {

class DataLoader
{
public:
    virtual ~DataLoader ();
    virtual void loadData () = 0;
    fm::GameLoop* m_gameLoop;
};

class XmlLoader : public DataLoader
{
public:
    XmlLoader (fm::GameLoop*, const char*);
    virtual ~XmlLoader ();
    static int strToInt (const char*);
    void loadData ();
private:
    const char* dataFilepath;
    tinyxml2::XMLElement * m_dataset;
    std::list<tinyxml2::XMLElement*> asList (
            tinyxml2::XMLElement*, const char*);
    void loadTextures ();
    void loadAnimations ();
    void loadCollidables ();
};


//class DataLoaderFactory
//{
//public:
//    DataLoaderFactory() {}

//    fm::XmlLoader * makeXmlLoader(fm::GameLoop, std::string);
//};

}
#endif // DATALOADER_H
