//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <utility>
#include <list>

#include <SFML/Graphics.hpp>

#include "GameLoop.h"
#include "Event.hpp"

#define noop


namespace fm = muncher;

//fm::GameLoop::GameLoop ()
//{
//    m_window = new sf::RenderWindow(
//                sf::VideoMode(800, 600), "Fruit Muncher");
//}

fm::Collidable* fm::GameLoop::getDudeman ()
{
    for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext())
    {
        fm::Collidable* c = static_cast<fm::Collidable*>(b->GetUserData());
        if (c->isDudeman())
        {
            return c;
        }
    }
    return 0;
}

void fm::GameLoop::setDudeman(fm::Collidable * c)
{
    m_dudeman = *c;
}

void fm::GameLoop::makeTexture (std::string filepath, std::string entityName)
{
    std::cout << "Initalizing texture " << entityName << std::endl;
    sf::Texture texture;
    texture.loadFromFile(filepath);
    this->textures[entityName] = texture;
}

void fm::GameLoop::loadTextures()
{
    sf::Texture tapir;
    tapir.loadFromFile("resources/tapir_test_strip1.png");
    this->textures["tapir"] = tapir;

    sf::Texture yellow;
    yellow.loadFromFile("resources/tapir_test_strip2.png");
    this->textures["yellow"] = yellow;
}

void fm::GameLoop::makeAnimation (
        std::string textureName,
        int rectangle[],
        int speed,
        int frames,
        std::string animationName)
{
    std::cout << "Initalizing sprite " << animationName << std::endl;
    sf::Texture t = this->textures[textureName];
    std::cout << "rectangule: " << rectangle[2] << ", " << rectangle[3] << std::endl;
    sf::IntRect r = sf::IntRect(rectangle[0],
            rectangle[1], rectangle[2], rectangle[3]);
    std::cout << "r: " << r.width << ", " << r.height << std::endl;
    fm::AnimationSequencer* seq = new fm::AnimationSequencer(speed, frames, r);
    fm::Animation * a = new fm::Animation(seq, t);
    this->animations[animationName] = a;
    std::cout << "animation frame: " << a->sequencer->getFrameSize().x << ", " << a->sequencer->getFrameSize().y << std::endl;
}

void fm::GameLoop::makeAnimations ()
{
    std::cout << "making animations" << std::endl;
//    sf::Texture t = this->textures["tapir"];
//    sf::IntRect r = sf::IntRect(0, 0, 100, 100);
//    fm::AnimationSequencer* seq = new fm::AnimationSequencer(7, 4, r);
//    fm::Animation * a = new fm::Animation(seq, t);
//    this->animations["tapir_run"] = a;

//    sf::Texture ty = this->textures["yellow"];
//    sf::IntRect ry = sf::IntRect(0, 0, 100, 100);
//    fm::AnimationSequencer* seqy = new fm::AnimationSequencer(7, 4, ry);
//    fm::Animation * ay = new fm::Animation(seqy, ty);
//    this->animations["tapir_yellow"] = ay;
}


void fm::GameLoop::makeCollidable(
        sf::Vector2f position,
        sf::Vector2f size,
        std::string type,
        std::string animation,
        float density,
        float friction,
        bool isDudeman)
{
    Collidable* c = new Collidable();
    c->type = type;
    //c->setAnimation(this->animations[animation]);
    b2BodyDef b_def = fm::CollidableBuilder::makeBodyDef(
                position.x, position.y, type);
    std::cout << "body def: " << &b_def << std::endl;
    std::cout << b_def.type << std::endl;
    std::cout << b_def.awake << std::endl;
    std::cout << b_def.position.x << ", " << b_def.position.y << std::endl;
    b2FixtureDef f_def = fm::CollidableBuilder::makeFixtureDef(
                density, friction, size.x, size.y);
    std::cout << "FixtureDef created" << std::endl;
    //b2BodyDef fake_def = b2BodyDef();
    //fake_def.position.Set(1.0f, 1.0f);
    b2Body* b = m_world->CreateBody(&b_def);
    //b2Body* b = m_world->CreateBody(&fake_def);
    std::cout << "b2Body* created" << std::endl;
    b->CreateFixture(&f_def);
    c->body = b;
    //b->SetUserData(c);
    sf::Sprite* s = new sf::Sprite();
    //s->setTexture(c->animation->texture);
    //sf::Sprite s(c->animation->texture);
    c->setSprite(s);
    c->setAnimation(this->animations[animation]);
    c->transform = this->getLocalTransform();
    if (isDudeman == true)
    {
        std::cout << "DUDEMAN!!!" << std::endl;
        c->setDudeman(true);
    }
    else
    {
        c->setDudeman(false);
        std::cout << "NO DUDEMAN!!!" << std::endl;
    }
    b->SetUserData(c);
    std::cout << "Collidable init success" << std::endl;
}

bool fm::GameLoop::popEvent(fm::Event& event)
{
    if (eventQueue.empty())
    {
        processEvents();
    }
    if (!eventQueue.empty())
    {
    event = eventQueue.front();
    eventQueue.pop();
    return true;
    }
    return false;
}

bool fm::GameLoop::pollEvent(fm::Event& event)
{
    if (popEvent(event))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void fm::GameLoop::pushEvent(fm::Event event)
{
    this->eventQueue.push(event);
}


// this method is really more handling events than processing them.
void fm::GameLoop::processInputEvents(sf::Event event)
{
    //sf::Event event;
    //m_window->pollEvent(event);
    if (event.type == sf::Event::KeyPressed)
    {
        sf::Keyboard::Key key = event.key.code;
        switch (key)
        {
                case sf::Keyboard::Key::Up :
                std::cout << "Jump command received" << std::endl;
                fm::Event jump;
                jump.type = fm::Event::EventType::InputEvent;
                jump.jump = fm::Event::InputJump();
                jump.jump.collidable = &m_dudeman;
                this->pushEvent(jump);
            break;

                case sf::Keyboard::Key::Right :
                std::cout << "Move command received" << std::endl;
                fm::Event right;
                right.type = fm::Event::EventType::InputEvent;
                right.move = fm::Event::InputMove();
                right.move.direction = 'r';
                right.move.collidable = &m_dudeman;
                this->pushEvent(right);
            break;

                case sf::Keyboard::Key::Left :
                std::cout << "Move command received" << std::endl;
                fm::Event left;
                left.type = fm::Event::EventType::InputEvent;
                left.move = fm::Event::InputMove();
                left.move.direction = 'l';
                left.move.collidable = &m_dudeman;
                this->pushEvent(left);
            break;
        default:
            noop;
        }
    }
}

void fm::GameLoop::processEvents()
{
    //this->processInputEvents();
}


// TODO: parameterize scale
sf::Transform fm::GameLoop::getLocalTransform()
{
    sf::Transform t;
    float s = m_scale;
    float t1 = m_worldSize.x * s;
    float t2 = m_worldSize.y * s;
    t = t.translate(t1, t2).scale(s, s).rotate(180.0f);
    std::cout << "t1, t2: " << t1 << " . " << t2 << std::endl;
    return t;
}

void fm::GameLoop::initWorld (
        float vertGravity,
        float scale,
        float x_worldSize,
        float y_worldSize)
{
    b2Vec2 gravity = b2Vec2(0.0f, vertGravity);
    b2World * wd = new b2World(gravity);
    m_world = wd;
    m_scale = scale;
    b2Vec2 size = b2Vec2(x_worldSize, y_worldSize);
    this->setWorldSize(size);
}

void fm::GameLoop::runLoop()
{
    //m_window->setVerticalSyncEnabled(true);
    m_window->setFramerateLimit(40);

    int elapsedCalls = 0;
    int elapsedCallsPerDisplay = 0;

    sf::View view;
    view.setSize(sf::Vector2f(800.0f, 600.0f));
    m_window->setView(view);

    sf::Clock frameClock;
    sf::Clock physicsClock;
    float physicsDeltaT = 0.0f;

    while (m_window->isOpen())
    {

        sf::Event event;
        while (m_window->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                m_window->close();
                //std::cout << elapsedCalls << std::endl;
            }
        }

        // apply events to bodies

        // more generally, `runLoop()` will execute in one thread,
        // while another thread will handle polling for events, so
        // then the application can take events from a console and/or
        // network interface.
        // This is probably where `runLoop` will yield the thread to
        // allow input buffers to do their thing.

        // checking the clock
        //sf::Time frameElapsed = frameClock.getElapsedTime();
        //sf::Time physicsElapsed = physicsClock.getElapsedTime();

        m_world->Step(this->physicsRate, 10, 8);

        //if (frameElapsed.asSeconds() - physicsDeltaT > this->physicsRate)
        //{
        //    physicsDeltaT = frameElapsed.asSeconds();
        //    m_world->Step(this->physicsRate, 10, 8);
        //}

        //if (frameElapsed.asSeconds() > this->graphicsRate)
        //{
            //std::cout << elapsed.asSeconds() << std::endl;

            // muncher event queue captures events from physics sim

            // drawing graphics

            m_window->clear(sf::Color::Black);
            for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext())
            {
                fm::Collidable* c = static_cast<fm::Collidable*>(b->GetUserData());
                c->applyAnimation();
                c->applyPosition();
                c->adjustOrigin();
                sf::FloatRect bounds;
                bounds = c->getSprite()->getLocalBounds();
                c->getSprite()->setOrigin(bounds.width/2, bounds.height/2);

                if (c->isDudeman())
                {
                    view.setCenter(c->getSprite()->getPosition());
                }
                sf::Sprite s = *c->getSprite();
                m_sprites.push(s);
                //m_window->draw(*c->getSprite());
            }

            m_window->setView(view);

            while (!m_sprites.empty())
            {
                m_window->draw(m_sprites.front());
                m_sprites.pop();
            }

            //std::cout << elapsedCallsPerDisplay << std::endl;
            //elapsedCallsPerDisplay = 0;

            //elapsed = clock.restart();
            //physicsPrevTime = 0.0f;
            std::cout << "tick" << std::endl;
            //frameElapsed = frameClock.restart();
        //}
        m_window->display();
    }
}


