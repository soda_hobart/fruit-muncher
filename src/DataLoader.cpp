#include "DataLoader.h"

namespace fm = muncher;

/*virtual*/ fm::DataLoader::~DataLoader() {}

fm::XmlLoader::XmlLoader(fm::GameLoop* gameLoop, const char* filepath)
{
    m_gameLoop = gameLoop;
    this->dataFilepath = filepath;
}

fm::XmlLoader::~XmlLoader() {}

int fm::XmlLoader::strToInt(const char* str)
{
    std::stringstream strVal;
    strVal << str;
    int intVal;
    strVal >> intVal;
    return intVal;
}

std::list<tinyxml2::XMLElement*> fm::XmlLoader::asList (
        tinyxml2::XMLElement* parent, const char* tag)
{
    std::list<tinyxml2::XMLElement*> output;
    std::cout << "retreiving first item" << std::endl;
    tinyxml2::XMLElement* item = parent->FirstChildElement(tag);
    std::cout << "retrieved first item" << std::endl;
    for (item; item->NextSiblingElement() != 0; item = item->NextSiblingElement())
    {
        output.push_back(item);
    }
    // not sure how to set the terminator to include the last one in the loop?
    tinyxml2::XMLElement* last = parent->LastChildElement();
    output.push_back(last);
    return output;
}

void fm::XmlLoader::loadTextures ()
{
    std::cout << "loading textures element" << std::endl;
    tinyxml2::XMLElement * texturesElement = m_dataset->FirstChildElement("textures");
    std::cout << m_dataset << std::endl;
    std::cout << "first element" << m_dataset->FirstChildElement()->Name() << std::endl;
    std::cout << "loading textures list" << std::endl;
    std::cout << texturesElement << std::endl;
    std::list<tinyxml2::XMLElement*> textures = asList(texturesElement, "texture");
    std::cout << "processing textures list" << std::endl;
    for (auto i : textures)
    {
        std::cout << "loading texture" << std::endl;
        tinyxml2::XMLElement* name = i->FirstChildElement("name");
        std::string str_name = (std::string)name->GetText();

        tinyxml2::XMLElement* filepath = i->FirstChildElement("filepath");
        std::string str_filepath = filepath->GetText();

        std::cout << str_filepath << std::endl;

        m_gameLoop->makeTexture(str_filepath, str_name);
    }
}

void fm::XmlLoader::loadAnimations ()
{
    tinyxml2::XMLElement * animationsElement = m_dataset->FirstChildElement("animations");
    std::list<tinyxml2::XMLElement*> animations = asList(animationsElement, "animation");
    for (auto i : animations)
    {
        // texture
        tinyxml2::XMLElement* textureName = i->FirstChildElement("textureName");
        std::string str_textureName = textureName->GetText();

        // animation name
        tinyxml2::XMLElement* name = i->FirstChildElement("name");
        std::string str_name = (std::string)name->GetText();

        // texture rect
        tinyxml2::XMLElement* elmt_rect = i->FirstChildElement("rectangle");
        int left, top, right, bottom;
        elmt_rect->QueryIntAttribute("left", &left);
        elmt_rect->QueryIntAttribute("top", &top);
        elmt_rect->QueryIntAttribute("right", &right);
        elmt_rect->QueryIntAttribute("bottom", &bottom);
        std::cout << "bottom: " << bottom << std::endl;
        int rect[4] {left, top, right, bottom};
        std::cout << "rectang: " << rect[2] << ", " << rect[3] << std::endl;

        tinyxml2::XMLElement* frames = i->FirstChildElement("frames");
        int int_frames;
        frames->QueryIntText(&int_frames);

        tinyxml2::XMLElement* speed = i->FirstChildElement("speed");
        int int_speed;
        speed->QueryIntText(&int_speed);

        m_gameLoop->makeAnimation(
                    str_textureName,
                    rect,
                    int_speed,
                    int_frames,
                    str_name);
    }

}

void fm::XmlLoader::loadCollidables()
{
    tinyxml2::XMLElement* collidablesElement = m_dataset->FirstChildElement("collidables");
    std::list<tinyxml2::XMLElement*> collidables = asList(collidablesElement, "collidable");

    for (auto i : collidables)
    {
        std::string str_type = i->FirstChildElement(
                    "type")->GetText();

        tinyxml2::XMLElement* pos = i->FirstChildElement("position");
        float pos_x;
        pos->QueryFloatAttribute("x", &pos_x);
        float pos_y;
        pos->QueryFloatAttribute("y", &pos_y);
        sf::Vector2f vec_pos = sf::Vector2f(pos_x, pos_y);
        std::cout << "pos: " << vec_pos.x << ", " << vec_pos.y << std::endl;

        tinyxml2::XMLElement* size = i->FirstChildElement("size");
        float size_x;
        size->QueryFloatAttribute("width", &size_x);
        float size_y;
        size->QueryFloatAttribute("height", &size_y);
        sf::Vector2f vec_size = sf::Vector2f(size_x, size_y);
        std::cout << "size: " << vec_size.x << ", " << vec_size.y << std::endl;

        std::string str_animation = i->FirstChildElement(
                    "animationName")->GetText();
        std::cout << str_animation << std::endl;

        float density;
        i->FirstChildElement("density")->QueryFloatText(&density);

        float friction;
        i->FirstChildElement("friction")->QueryFloatText(&friction);

        bool isDudeman = i->FirstChildElement(
                    "isDudeman")->QueryBoolText(&isDudeman);

        std::cout << "dudemanity: " << isDudeman << std::endl;

        m_gameLoop->makeCollidable(
                    vec_pos,
                    vec_size,
                    str_type,
                    str_animation,
                    density,
                    friction,
                    isDudeman);
    }
}


/* virtual */
void fm::XmlLoader::loadData ()
{
    tinyxml2::XMLDocument doc;
    doc.LoadFile(this->dataFilepath);
    m_dataset = doc.FirstChildElement();
    loadTextures();
    loadAnimations();
    loadCollidables();
    // probably lots of memory leaks
    //delete this;
}


