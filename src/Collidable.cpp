//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include "Collidable.h"

namespace fm = muncher;

b2Vec2 fm::Collidable::getWorldPosition()
{
    return this->body->GetPosition();
}

void fm::Collidable::adjustOrigin ()
{
    sf::FloatRect b = m_sprite->getLocalBounds();
    m_sprite->setOrigin(b.width/2, b.height/2);
}


void fm::Collidable::setDudeman (bool dudemanity)
{
    m_dudemanity = dudemanity;
}


void fm::Collidable::setLocalPosition(b2Vec2 pos)
{
    float x = pos.x;
    float y = pos.y;    
    sf::Vector2f point = this->transform.transformPoint(x, y);
    //std::cout << "transformed point: " << point.x << ", " << point.y << std::endl;
    sf::FloatRect b = m_sprite->getLocalBounds();
    //m_sprite.setOrigin(b.width/2, b.height/2);
    this->adjustOrigin();
    m_sprite->setPosition(point);
}

void fm::Collidable::applyPosition ()
{
    b2Vec2 pos = this->getWorldPosition();
    //std::cout << "world position is: " << pos.x << ", " << pos.y << std::endl;
    this->setLocalPosition(pos);
    sf::Vector2f localPos = m_sprite->getPosition();
    //std::cout << "local position is: " << localPos.x << ", " << localPos.y << std::endl;
}

void fm::Collidable::applyAnimation ()
{
    sf::IntRect frame;
    frame = this->animation->sequencer->getFramePosition();
    //std::cout << frame.width << ", " <<frame.height << std::endl;
    m_sprite->setTextureRect(frame);
    //std::cout << "texture rect: " << m_sprite->getTextureRect().left << std::endl;
}

void fm::Collidable::setAnimation(fm::Animation* a)
{
    this->animation = a;
    m_sprite->setTexture(a->texture);
    this->applyAnimation();
}


