//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#ifndef EVENT_H
#define EVENT_H

#include "Collidable.h"

namespace muncher
{

class Event
{
public:

    struct InputJump
    {
        Collidable * collidable;
    };
    struct InputMove
    {
        Collidable * collidable;
        char direction;
    };
    struct PhysicsCollision
    {
        Collidable * c1;
        Collidable * c2;
    };

    enum EventType
    {
        InputEvent,
        ScriptedEvent,
        PhysicsEvent
    };

    EventType type;

    union {
        InputJump jump;
        InputMove move;
        PhysicsCollision collision;
    };
};

//end namespace muncher
}
#endif // EVENT_H
