//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#ifndef COLLIDABLE_H
#define COLLIDABLE_H

#include <string>

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

#include "Animation.h"

namespace fm = muncher;

namespace muncher
{

class Collidable
{
public:
    Collidable() {}
    virtual ~Collidable () {}
    b2Body * body;
    b2Fixture * fixture;
    std::string type;
    fm::Animation * animation;
    sf::Transform transform;

    bool isDudeman () {return m_dudemanity;}
    void setDudeman (bool);

    // wrt and rendering
    b2Vec2 getWorldPosition();
    void setLocalPosition(b2Vec2);
    void applyPosition();
    void adjustOrigin();

    // animation and texture drawing
    void setAnimation (fm::Animation*);
    void applyAnimation ();

    sf::Sprite* getSprite () {return m_sprite;}
    void setSprite(sf::Sprite* s)
    {
        m_sprite = s;
    }

private:
    sf::Sprite * m_sprite;
    bool m_dudemanity;
};

}
#endif // COLLIDABLE_H
