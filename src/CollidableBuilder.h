//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#ifndef COLLIDABLEBUILDER_H
#define COLLIDABLEBUILDER_H

#include <string>

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

#include "Collidable.h"

namespace fm = muncher;

namespace muncher {

class CollidableBuilder
{
public:
    static b2BodyDef makeBodyDef (float, float, std::string);
    static b2FixtureDef makeFixtureDef (
            float, float, float, float);
};

}
#endif // COLLIDABLEBUILDER_H
