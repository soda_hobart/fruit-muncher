#include <iostream>

#include "GameLoop.h"
#include "DataLoader.h"

namespace fm = muncher;

int main (int argc, char* argv[])
{
    fm::GameLoop g;
    g.initWorld(-9.0f, 10.0f, 1000.0f, 1000.0f);
    fm::XmlLoader xmlLoader = fm::XmlLoader(&g, argv[1]);
    xmlLoader.loadData();
    g.runLoop();
}
