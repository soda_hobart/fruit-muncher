//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#include <SFML/Graphics.hpp>
#include "Animation.h"

namespace fm = muncher;

fm::AnimationSequencer::AnimationSequencer (int speed,
              int frames,
              sf::IntRect position)
{
  m_speed = speed;
  m_frames = frames;
  m_currentFrame = 0;
  m_timestepCounter = 0;
  m_initialPosition = position;
  m_framePosition = position;
  m_frameSize = sf::Vector2f(position.width, position.height);
}

// private
void fm::AnimationSequencer::incrFrameCounter ()
{
  if (this->m_frameCounter < this->getFrames())
    {
      this->m_frameCounter++;
      this->advanceFramePosition();
    }
  else
    {
      this->m_frameCounter = 0;
      this->resetFramePosition();
    }
}

void fm::AnimationSequencer::resetFrameCounter ()
{
  this->m_frameCounter = 0;
}

void fm::AnimationSequencer::incrTimestepCounter ()
{
  if (this->m_timestepCounter < this->getSpeed())
    {
      this->m_timestepCounter++;
    }
  else
    {
      this->m_timestepCounter = 0;
      this->incrFrameCounter();
    }
}

// public

void fm::AnimationSequencer::advanceFramePosition ()
{
  float offsetX = this->m_framePosition.left + this->m_frameSize.x;
  sf::IntRect nextPos(offsetX,
			this->m_framePosition.top,
			this->m_frameSize.x,
			this->m_frameSize.y);
  this->m_framePosition = nextPos;
}

void fm::AnimationSequencer::resetFramePosition ()
{
  this->m_framePosition = this->m_initialPosition;
}

sf::IntRect fm::AnimationSequencer::getFramePosition ()
{
  this->incrTimestepCounter();
  return this->m_framePosition;
}

fm::Animation::Animation(AnimationSequencer * s, sf::Texture& t)
{
    sequencer = s;
    texture = t;
}
