//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>

namespace muncher
{

class AnimationSequencer
{
 private:
  int m_speed; //how many ticks to hold frame for
  int m_frames; //length of filmstrip on sprite sheet
  int m_currentFrame;
  int m_frameCounter;
  int m_timestepCounter;
  sf::Vector2f m_frameSize;
  // TODO: do I need to delete Texture pointer in destructor?
  // Probably don't want to, because this way, many AnimationSequencers
  // can point to the same texture, so we only have to load
  // the texture once but refer back to it at leisure.  Textures
  // and their pointers can be cleaned up by their own logic.
  sf::IntRect m_initialPosition;
  sf::IntRect m_framePosition;
  //rectangular selection from sprite sheet
  
 public:
  AnimationSequencer (int, int, sf::IntRect);
  ~AnimationSequencer () {}

  int getSpeed () {return m_speed;}
  void setSpeed (int s) {m_speed = s;}
  
  int getFrames () {return m_frames;}
  void setFrames (int f) {m_frames = f;}

  int getCurrentFrame () {return m_currentFrame;}
  void setCurrentFrame (int f) {m_currentFrame = f;}

  sf::Vector2f getFrameSize () {return m_frameSize;}
  void setFrameSize(sf::Vector2f s) {m_frameSize = s;}

  void advanceFramePosition ();
  void resetFramePosition ();

  void incrFrameCounter ();
  void resetFrameCounter ();
  
  void incrTimestepCounter ();
  
  sf::IntRect getFramePosition ();
  // the payload.
};


// Associates an AnimationSequencer with a Texture
class Animation
{
public:
    Animation(AnimationSequencer*, sf::Texture&);
    sf::Texture texture;
    AnimationSequencer * sequencer;
};

}

#endif // ANIMATION_H
