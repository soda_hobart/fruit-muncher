//    This file is part of FRUIT MUNCHER.

//    FRUIT MUNCHER is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    FRUIT MUNCHER is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with FRUIT MUNCHER.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <string>

#include "CollidableBuilder.h"

namespace fm = muncher;

b2BodyDef fm::CollidableBuilder::makeBodyDef(
        float x, float y, std::string type)
{
    b2BodyDef b;
    if (type == "kinematic")
    {
        b.type = b2_kinematicBody;
    }
    else if (type == "static")
    {
        b.type = b2_staticBody;
    }
    else
    {
        b.type = b2_dynamicBody;
    }
    b.angularDamping = 0.01f;
    b.active = true;
    b2Vec2 pos = b2Vec2(x, y);
    b.position = pos;
    std::cout << "makin a tapir" << std::endl;
    std::cout << sizeof(b2Body) << std::endl;
    return b;
}

b2FixtureDef fm::CollidableBuilder::makeFixtureDef(
        float density, float friction, float w, float h)
{
    b2FixtureDef f;
    b2PolygonShape* box = new b2PolygonShape();
    box->SetAsBox(w, h);
    f.shape = box;
    f.density = density;
    f.friction = friction;
    return f;
}
